#' Cosine (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of cosine for each element of \code{x} in degrees in a
#' manner compatible with GNU Octave/MATLAB. Zero is returned for any "elements
#' where (\code{x} - 90) / 180 is an integer." Reference: Eaton.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The cosine of each element of \code{x} in degrees. Zero for any
#' "elements where (\code{x} - 90) / 180 is an integer."
#'
#'
#'
#'
#' @references
#' \enumerate{
#'    \item John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 358.
#'    \item Wikimedia Foundation, Inc. Wikipedia, 24 February 2019, "Radian", \url{https://en.wikipedia.org/wiki/Radian}.
#'
#'
#'
#' @author David Bateman (GNU Octave cosd), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave cosd
#' cosd(seq(0, 80, by = 10))
#'
#' cosd(c(0, 180, 360))
#'
#' cosd(c(90, 270, 45))
#'
#' cosd(pi * seq(0, 80, by = 10) / 180) # gives error message since the computed value is in radians rather than degrees
#'
#' cos(pi * seq(0, 80, by = 10) / 180) # this is correct since `cos` expects the angle in radians
#'
#' cosd(seq(0, 80, by = 10) * 180 / pi) # converts angles in radians to degrees; however, it will still receive an error message with this current implementation. You can use the work-around below:
#' xx <- seq(0, 80, by = 10) * 180 / pi
#' cosd(xx)
#'
#' cos(seq(0, 80, by = 10) * 180 / pi) # converts angles in radians to degrees; however, this is incorrect since `cos` expects the angle in radians and not degrees
#'
#' cosd("90")
#'
#'
#'
#'
#'
#' @importFrom pracma Fix
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#'
#'
#' @export
cosd <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")



  I <- quoted_eval(x) / 180

  y <- cos (I * pi)

  I <- I + 0.5

  y <- ifelse(I == Fix(I) & is.finite(I), 0, y)

  return(y)

}




#' Inverse cosine (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of inverse cosine for each element of \code{x} in
#' degrees in a manner compatible with GNU Octave/MATLAB.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The inverse cosine of each element of \code{x} in degrees.
#'
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 359.
#'
#'
#'
#' @author David Bateman (GNU Octave acosd), Irucka Embry
#'

#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave acosd
#' acosd (seq(0, 1, by = 0.1))
#'
#'
#' @importFrom pracma Fix
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#' @export
acosd <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")



  y <- acos (quoted_eval(x)) * 180 / pi

  return(y)

}



#' Sine (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of sine for each element of \code{x} in degrees in a
#' manner compatible with GNU Octave/MATLAB. Zero is returned for any "elements
#' where \code{x} / 180 is an integer." Reference: Eaton.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The sine of each element of \code{x} in degrees. Zero for any
#' "elements where \code{x} / 180 is an integer."
#'
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 358.
#'
#'
#'
#' @author David Bateman (GNU Octave sind), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave sind
#' sind(seq(10, 90, by = 10))
#'
#' sind(c(0, 180, 360))
#'
#' sind(c(90, 270))
#'
#'
#' @importFrom pracma Fix
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#' @export
sind <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")


I <- quoted_eval(x) / 180

y <- sin (I * pi)

y <- ifelse(I == Fix(I) & is.finite(I), 0, y)

return(y)

}



#' Inverse sine (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of inverse sine for each element of \code{x} in degrees
#' in a manner compatible with GNU Octave/MATLAB.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The inverse sine of each element of \code{x} in degrees.
#'
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 359.
#'
#'
#'
#' @author David Bateman (GNU Octave asind), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave asind
#' asind (seq(0, 1, by = 0.1))
#'
#'
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#' @export
asind <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")


  y <- asin (quoted_eval(x)) * 180 / pi

  return(y)

}




#' Tangent (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of tangent for each element of \code{x} in degrees in a
#' manner compatible with GNU Octave/MATLAB. Zero is returned for any "elements
#' where \code{x} / 180 is an integer and \code{Inf} for elements where
#' (\code{x} - 90) / 180 is an integer." Reference: Eaton.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The tangent of each element of \code{x} in degrees. Zero for any
#' "elements where \code{x} / 180 is an integer and \code{Inf} for elements where
#' (\code{x} - 90) / 180 is an integer."
#'
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 358.
#'
#'
#'
#' @author David Bateman (GNU Octave tand), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave tand
#' tand(seq(10, 80, by = 10))
#'
#' tand(c(0, 180, 360))
#'
#' tand(c(90, 270))
#'
#'
#' @importFrom pracma Fix
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#' @export
tand <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")



  I0 <- quoted_eval(x) / 180

  I90 <- (quoted_eval(x) - 90) / 180

  y <- tan (I0 * pi)

y <- ifelse(I0 == Fix(I0) & is.finite(I0), 0, y)

y <- ifelse(I90 == Fix(I90) & is.finite(I90), Inf, y)

  return(y)

}



#' Inverse tangent (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of inverse tangent for each element of \code{x} in
#' degrees in a manner compatible with GNU Octave/MATLAB.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The inverse tangent of each element of \code{x} in degrees.
#'
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 359.
#'
#'
#'
#' @author David Bateman (GNU Octave atand), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave atand
#' atand (seq(0, 90, by = 10))
#'
#'
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#' @export
atand <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")


  y <- 180 / pi * atan (quoted_eval(x))

  return(y)

}



#' Secant (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of secant for each element of \code{x} in degrees in a
#' manner compatible with GNU Octave/MATLAB.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The secant of each element of \code{x} in degrees.
#'
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 358.
#'
#'
#'
#' @author David Bateman (GNU Octave secd), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave secd
#' secd (seq(0, 80, by = 10))
#'
#' secd (c(0, 180, 360))
#'
#' secd (c(90, 270))
#'
#'
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#' @export
secd <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")


    y <- 1 / cosd (quoted_eval(x))

  return(y)

}




#' Inverse secant (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of inverse secant for each element of \code{x} in
#' degrees in a manner compatible with GNU Octave/MATLAB.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The inverse secant of each element of \code{x} in degrees.
#'
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 358.
#'
#'
#'
#' @author David Bateman (GNU Octave asecd), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave asecd
#' asecd (seq(0, 90, by = 10))
#'
#'
#'
#' @importFrom pracma asec
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#'
#' @export
asecd <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")


    y <- asec (quoted_eval(x)) * 180 / pi

  return(y)

}



#' Cosecant (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of cosecant for each element of \code{x} in degrees in a
#' manner compatible with GNU Octave/MATLAB.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The cosecant of each element of \code{x} in degrees.
#'
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 358.
#'
#'
#'
#' @author David Bateman (GNU Octave cscd), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave cscd
#' cscd (seq(0, 90, by = 10))
#'
#' cscd (c(0, 180, 360))
#'
#' cscd (c(90, 270))
#'
#'
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#' @export
cscd <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")


    y <- 1 / sind (quoted_eval(x))

  return(y)

}



#' Inverse cosecant (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of inverse cosecant for each element of \code{x} in
#' degrees in a manner compatible with GNU Octave/MATLAB.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The inverse cosecant of each element of \code{x} in degrees.
#'
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 359.
#'
#'
#'
#' @author David Bateman (GNU Octave acscd), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave acscd
#' acscd (seq(0, 90, by = 10))
#'
#'
#' @importFrom pracma acsc
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#'
#' @export
acscd <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")


    y <- acsc (quoted_eval(x)) * 180 / pi

  return(y)

}





#' Cotangent (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of inverse secant for each element of \code{x} in
#' degrees in a manner compatible with GNU Octave/MATLAB.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The inverse secant of each element of \code{x} in degrees.
#'
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 358.
#'
#'
#'
#' @author David Bateman (GNU Octave cotd), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave cotd
#' cotd (seq(0, 80, by = 10))
#'
#' cotd (c(0, 180, 360))
#'
#' cotd (c(90, 270))
#'
#'
#'
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#' @export
cotd <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")



    y <- 1 / tand (quoted_eval(x))

  return(y)

}




#' Inverse cotangent (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of inverse cotangent for each element of \code{x} in
#' degrees in a manner compatible with GNU Octave/MATLAB.
#'
#' @param x A numeric vector containing values in degrees
#'
#' @return The inverse cotangent of each element of \code{x} in degrees.
#'
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 359.
#'
#'
#'
#' @author David Bateman (GNU Octave acotd), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave acotd
#' acotd (seq(0, 90, by = 10))
#'
#'
#'
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
#' @export
acotd <- function (x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")



    y <- atand (1 / quoted_eval(x))

  return(y)

}





#' "Two-argument arc-tangent" (in degrees) [GNU Octave/MATLAB compatible]
#'
#' Calculates the value of the "two-argument arc-tangent" for each element of
#' (\code{y}, \code{x}) in degrees in a manner compatible with GNU Octave/MATLAB.
#'
#' @param x A numeric vector containing values in degrees
#' @param y A numeric vector containing values in degrees
#'
#' @return The "two-argument arc-tangent" of each element of (\code{y}, \code{x})
#'         in degrees. Note: "The arc-tangent of two arguments atan2(y, x)
#'         returns the angle between the x-axis and the vector from the origin
#'         to (x, y), i.e., for positive arguments atan2(y, x) == atan(y/x)."
#'         Source: \code{Trig} (base).
#'
#'
#'
#' @references
#' John W. Eaton, David Bateman, and Søren Hauberg (2009). \emph{GNU Octave version 3.0.1 manual: a high-level interactive language for numerical computations}. CreateSpace Independent Publishing Platform. ISBN 1441413006, URL \url{http://www.gnu.org/software/octave/doc/interpreter/}. Page 358.
#'
#'
#'
#' @author Rik Wehbring (GNU Octave atan2d), Irucka Embry
#'
#'
#'
#' @encoding UTF-8
#'
#'
#'
#'
#'
#' @examples
#' library("iemisc")
#'
#' # Examples from GNU Octave atan2d
#' atan2d (a <- seq(-1, 1, by = 0.1), b <- seq(1, -1, by = -0.1))
#'
#'
#'
#' @import assertthat
#' @import quotedargs
#' @import stringi
#'
# #' @export
atan2d <- function (y, x) {

quoted_arg(x)

# Check for x
assert_that(not_empty(deparse(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(!(is.string(notquoted(x))), msg = "The object `x` is not a number. `x` cannot be a string. Please try again.")
# only process with a numeric value for x and provide a stop warning if x is a string

assert_that(noNA(deparse(x)), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

if (stri_detect_fixed(deparse(x), "pi")) {
	cat ("This is the object `x`:", deparse(x), "\n")
    }

assert_that(!stri_detect_fixed(deparse(x), "pi"), msg = "The object `x` is probably a radian since it includes 'pi'. The object `x` should be in degrees and not radians. Please try again.")


quoted_arg(y)

# Check for y
assert_that(not_empty(deparse(y)), msg = "y is empty. Please try again.")
# only process with a value for y and provide a stop warning if y is empty

assert_that(!(is.string(notquoted(y))), msg = "The object `y` is not a number. `y` cannot be a string. Please try again.")
# only process with a numeric value for y and provide a stop warning if y is a string

assert_that(noNA(deparse(y)), msg = "y is NA. Please try again.")
# only process with a non-NA value for y and provide a stop warning if y contains NA

if (stri_detect_fiyed(deparse(y), "pi")) {
	cat ("This is the object `y`:", deparse(y), "\n")
    }

assert_that(!stri_detect_fiyed(deparse(y), "pi"), msg = "The object `y` is probably a radian since it includes 'pi'. The object `y` should be in degrees and not radians. Please try again.")




    z <- 180 / pi * atan2 (quoted_eval(y), quoted_eval(x))

  return(z)

}
