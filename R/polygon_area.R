#' Construction fraction
#'
#' Convert a construction measurement in US Customary Units (foot + inch) with
#' or without a fraction into its equivalent in feet only (with a decimal value
#' for the inches)
#'
#'
#' @param measurement character vector that contains the construction
#'     measurement (foot + inch)


Parameters
X, Y 	Arrays of the x and y coordinates of the vertices, traced in a clockwise direction, starting at any vertex. If you trace them counterclockwise, the result will be correct but have a negative sign.
numPoints 	The number of vertices
#'
#'
#'
#' @return the construction measurement value as a numeric vector with the feet
#'     only (with a decimal value for the inches) Returns 	the area of the polygon
#'


#'
#'
#'
#' @return the construction measurement value as a numeric vector with the feet
#'     only (with a decimal value for the inches)
#'
#'
#'
#'
#' @source
#' \enumerate{
#'    \item r - Convert a character vector of mixed numbers, fractions, and integers to numeric - Stack Overflow answered by G. Grothendieck on May 20 2012 and edited on May 21 2012. See \url{https://stackoverflow.com/questions/10674992/convert-a-character-vector-of-mixed-numbers-fractions-and-integers-to-numeric}.
#'    \item removing all non-numeric characters from a string, but not "." - R help on nabble.com answered by David Winsemius on Jul 26, 2016. See \url{http://r.789695.n4.nabble.com/removing-all-non-numeric-characters-from-a-string-but-not-quot-quot-td4723146.html}.
https://www.myengineeringworld.net/2014/06/polygon-area-shoelace-algorithm-Excel.html | My Engineering World: Calculating The Area Of A Simple Polygon Using The Shoelace Algorithm Written by Christos Samaras on Thursday, 26 June 2014

https://www.mathopenref.com/coordpolygonarea2.html
Math Open Reference: Algorithm to find the area of a polygon

https://www.alexejgossmann.com/benchmarking_r/
5 ways to measure running time of R code
Alexej Gossmann
Written on May 28, 2017

#'}
#'
#'
#'
#'
#' @author Darel Rex Finley (2006) for the original C code, Irucka Embry (R code)
#'
#'
#'
#' @encoding UTF-8
#'
#'
#' @seealso \code{\link[pracma]{polyarea}} for more robust area and length functions
#'
#
#'
#' @examples
#' library("iemisc")
#'
#' # Example 1
#' # from Source 2
#' x <- c(4,  4,  8,  8, -4, -4)
#' y <- c(6, -4, -4, -8, -8, 6)
#'
#' polygon_area(x, y)
#'
#'
#'
#' # Example 2
#'
#' type 38 <- construction_fraction("46'-10 1/2\"")
#'
#' x38 <- c(0, 25, sum(25, type38, 10), sum(25, type38, 10, 25))
#'
#' y38 <- c(0, rep((3 + 1 / 3), 2), 0)
#'
#' # Source 3
#' library("microbenchmark")
#'
#' check_for_equal_area <- function(values) {
#'   equality <- values[[1]] %==% values[[2]]
#' }
#'
#' mbm <- microbenchmark("polyarea" = {abs(pracma::polyarea(x38, y38))},
#'                "area" = {polygon_area(x, y)},
#'                check = check_for_equal_area)
#'
#' mbm
#'
#'
#'
#' # Example 3
#'
#' coords <- fread("
#' X,   Y
#' 0,	0
#' 34,	4
#' 58,	4
#' 84,	6.7
#' 184,	0", header = TRUE)
#'
#' polygon_area(coords$X, coords$Y)
#'
#'

# examples
Xx = c(0, 4, 4, 0); Yy = c(0, 0, 4, 4)
pracma::polyarea(Xx, Yy)
#Output : 16


Xx1 = c(0, 4, 2); Yy1 = c(0, 0, 4)
pracma::polyarea(Xx1, Yy1)
# Output : 8

#'
#'
#' @import fpCompare
#' @import assertthat
#'
#' @export
polygon_area <- function (x, y) {

# check on x
assert_that(!(is.empty(x)), msg = "x is empty. Please try again.")
# only process with a value for x and provide a stop warning if x is empty

assert_that(noNA(x), msg = "x is NA. Please try again.")
# only process with a non-NA value for x and provide a stop warning if x contains NA

assert_that(!(is.numeric(x)), msg = "x is a number, but it should be a string. Please try again.")
# only process with a string value for x and provide a stop warning if x is numeric



# check on y
assert_that(!(is.empty(y)), msg = "y is empty. Please try again.")
# only process with a value for y and provide a stop warning if y is empty

assert_that(noNA(y), msg = "y is NA. Please try again.")
# only process with a non-NA value for y and provide a stop warning if y contains NA

assert_that(!(is.numeric(y)), msg = "y is a number, but it should be a string. Please try again.")
# only process with a string value for y and provide a stop warning if y is numeric


# check on x and y
assert_that(length(x) %==% length(y), msg = "The length of x and y are not equal. Please check the x and y values and try again.")
# Source 1
# only process with a string value for y and provide a stop warning if y is numeric

assert_that(length(x) %==% 3, msg = "The length of x and y should at least be 3. The smallest polygon is a triangle with 3 vertices and sides. Please check the x and y values and try again.")
# Source 1
# only process with a string value for y and provide a stop warning if y is numeric


points <- length(x)

area <- 0 # Accumulates area in the loop # Source 2

i <- 0

j <- points

area <- vector("list", length(x))

for (i in seq_along(x)) {

     area[[i]] <- (x[[j]] + x[[i]]) * (y[[j]] - y[[i]])

	 j <- i
	 }

area1 <- abs(sum(unlist(area)) * 0.5)

return(area1)
}




#' @import data.table
#' @import ggplot2
plot_polygon <- function(x, y, fill = NULL, colour = NULL) {

fill <- fill

colour <- colour

ifelse(is.null(fill), fill <- "black", fill <- fill)

ifelse(is.null(colour), colour <- "black", colour <- colour)

# to plot the coordiantes
coords <- data.table(X = x, Y = y)

ggplot(coords, aes(X, Y)) + geom_polygon(fill = fill, colour = colour)

}
