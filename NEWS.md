# iemisc 0.9.8

* For the use of Table 3.3 (determining the water-cement ratio by weight) in the concr_mix_normal_strength function, `pracma`'s interp1 is now used for the cases where the compressive strength value is not present in the Table (see the Reference source for the Table)
* Added the ability to specify the trial batch volume for the concr_mix_normal_strength function
* Added the engr_survey for converting engineering survey measurements in Kentucky and Tennessee to decimal degrees
* Added the engr_survey2 for converting engineering survey measurements in Kentucky and Tennessee to decimal degrees
* Added the engr_survey3 for converting engineering survey measurements in Kentucky and Tennessee to decimal degrees
* Added the air_stripper function to size air strippers for removing volatile organic compounds from liquids
* Added the concr_mix_lightweight_strength [Concrete Mix Design for Structural Lightweight Concrete]
* Added the hydraulics function to
* Added `listless` as imported R packages for both the concr_mix_normal_strength and concr_mix_lightweight_strength functions
* Added `CHNOSZ` and `IAPWS95` as imported R packages for the air_stripper function
* Added  as imported R packages for the engr_survey function
* Now importing the `assertthat` package to test within functions
* Added the `testthat` package as a suggested R package for testing
* Changed the output table format for benefitcost from a `data.frame` to a `data.table`
* Revised the CITATION file
* Added both David Bateman and Rik Wehbring as author/contributors for their GNU Octave code

# iemisc 0.9.7

* CRAN fix of example from PgivenG

# iemisc 0.9.6

* Revised the vignette and changed the vignette filename and title
* Added `iemiscdata` and `import` as imported R packages (Issue #1 by jangorecki)
* Added `iemiscdata` and `import` as imports in functions as needed (Issue #1 by jangorecki)
* Added the concr_mix_normal_strength function [Concrete Mix Design for Normal Strength (Normal-weight) Concrete]
* Added `gsubfn` and `fpCompare` as imported R packages for the concr_mix_normal_strength function


# iemisc 0.9.5

* Added GNU Octave/MATLAB compatible trigonometric functions in degrees (`cosd`, `acosd`, `sind`, `asind`, `tand`, `atand`, `secd`, `asecd`, `cscd`, `acscd`, `cotd`, `acotd`, `atan2d`)


# iemisc 0.9.2

* Added `ie2misc`, `ie2miscdata` as suggested R packages


# iemisc 0.9.1

* Added more examples to the README.md
* Revised the examples in these functions: `Manningtri`, `Manningtrap`, `Manningrect`, `Manningpara`, `Manningcirc`
* Updated the Open Channel Flow problems vignette


# iemisc 0.9.0

* Updated these functions: `Manningtri`, `Manningtrap`, `Manningrect`, `Manningpara`, `Manningcirc`
* Added an Open Channel Flow problems vignette
* Added `install.load` as a suggested R package


# iemisc 0.5.2

* Updated these functions: `size`, `righttri`, `Manningtri`, `Manningtrap`, `Manningrect`, `Manningpara`, `Manningcirc`


# iemisc 0.5.1

* Renamed lengths to length_octave
* Updated these functions: `size`, `righttri`, `Manningtri`, `Manningtrap`, `Manningrect`, `Manningpara`, `Manningcirc`
* Added `iemiscdata` as a suggested R package


# iemisc 0.5.0

* Initial release
