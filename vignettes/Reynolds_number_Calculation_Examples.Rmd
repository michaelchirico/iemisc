---
title: "Calculating the Reynolds number Examples"
author: "Irucka Embry, E.I.T. (EcoC2S)"
date: "`r Sys.Date()`"
output:
html_document:
mathjax: default
---

<br />

Note: If you wish to replicate the R code below, then you will need to copy and paste the following commands in R first (to make sure you have all the packages and their dependencies):

```{r eval = FALSE, tidy = TRUE}
install.packages(c("install.load", "sessioninfo", "iemisc", "units", "IAPWS95", "fpCompare", "assertthat", "flextable"))
# install the packages and their dependencies, including the extra system dependencies (this process may take a while depending on the number of dependencies)
```

<br />
<br />

```{r, warning = FALSE, message = FALSE, tidy = TRUE}
# load the required packages and provide the session information
install.load::load_package("iemisc", "units", "IAPWS95", "fpCompare", "assertthat", "flextable")
# load needed packages using the load_package function from the install.load package (it is assumed that you have already installed these packages)


sessinfo <- setDT(sessioninfo::session_info()$packages)
sessinfo <- sessinfo[, -c("ondiskversion", "loadedpath", "path", "attached", "is_base", "md5ok", "library")]
setkey(sessinfo, package)
sessinfo
```

<br />
<br />

## Problem Statement

Problem 17.2 [Lindeburg Practice]

"Points A and B are separated by 3000 ft of new 6 in schedule-40 steel pipe. 750 gal/min of 60°F water flows from point A to point B. Point B is 60 ft above point A."

What is the Reynolds number?

From Appendix 16.B Dimensions of Welded and Seamless Steel Pipe [Lindeburg Manual], the internal diameter for a 6 inch nominal diameter new schedule-40 steel pipe is 0.5054 ft with an internal area of 0.2006 ft^2^.


From Table 17.2 Values of Specific Roughness for Common Pipe Materials [Lindeburg Manual], the specific roughness, $\epsilon$, for a steel pipe is 0.0002 ($2e-04$) ft.

<br />
<br />

## Solution

```{r, warning = FALSE, message = FALSE, tidy = TRUE}
# Please note that the Re1 and Re2 functions are found within the iemisc R package created by Irucka Embry


# 60 degrees Fahrenheit water
# new 6 in schedule-40 steel pipe
# determine the Reynolds number


# create unit of lbm which is commonly seen in engineering rather than lb for the mass unit
install_conversion_offset("lb", "lbm", 1)

# given the water flow of 750 gal / min
Vdot <- 750
Vdot

# create a numeric vector with the units of gallons per minute for the volumetric flow rate
units(Vdot) <- with(ud_units, "gallon/min")
Vdot

# create a numeric vector with the units of cubic feet per second for the volumetric flow rate
Vdot <- set_units(Vdot, "ft^3/sec")
Vdot

# given temperature of 60 degrees Fahrenheit
T <- 60
T

# create a numeric vector with the units of degrees Fahrenheit
T_F <- set_units(T, degree_F)
T_F

# create a numeric vector to convert from degrees Fahrenheit to Kelvin
T_K <- T_F
T_K

# create a numeric vector with the units of Kelvin
units(T_K) <- with(ud_units, K)
T_K

# saturated liquid density at 60 degrees Fahrenheit (SI units)
rho_SI <- DfT(drop_units(T_K))
rho_SI <- rho_SI * as_units("kg/m^3")
rho_SI

# saturated liquid density at 60 degrees Fahrenheit (US Customary units)
rho_Eng <- rho_SI
units(rho_Eng) <- with(ud_units, "lbm/ft^3")
rho_Eng

# kinematic viscosity at 60 degrees Fahrenheit and density of rho (SI units)
nu_SI <- KViscTD(drop_units(T_K), drop_units(rho_SI))
nu_SI <- nu_SI * as_units("m^2/s")
nu_SI

# kinematic viscosity at 60 degrees Fahrenheit and density of rho (US Customary units)
nu_Eng <- nu_SI
units(nu_Eng) <- with(ud_units, "ft^2/sec")
nu_Eng

# absolute or dynamic viscosity at 60 degrees Fahrenheit and density of rho (SI units)
mu_SI <- ViscTD(drop_units(T_K), drop_units(rho_SI))
mu_SI <- mu_SI * as_units("Pa*s")
mu_SI

# absolute or dynamic viscosity at 60 degrees Fahrenheit and density of rho (US Customary units)
mu_Eng <- mu_SI
units(mu_Eng) <- with(ud_units, "lbf*sec/ft^2")
mu_Eng

# create a numeric vector with the units of feet for the given specific roughness
epsilon <- 2e-04 * as_units("ft")
epsilon

# create a numeric vector with the units of feet for the given internal pipe diameter
Di <- 0.5054 * as_units("ft")
Di

# relative roughness (dimensionless) of the steel pipe
rel_roughness <- epsilon / Di
rel_roughness

# internal area of the steel pipe
Ai <- Di ^ 2 * pi / 4
Ai

# average velocity of the flowing water
V <- Vdot / Ai
V

# Reynolds number using the kinematic viscosity
Re_nu <- Re2(D = drop_units(Di), V = drop_units(V), nu = drop_units(nu_Eng))
Re_nu

# Reynolds number using the absolute or dynamic viscosity
Re_mu <- Re1(D = drop_units(Di), V = drop_units(V), rho = drop_units(rho_Eng), mu = drop_units(mu_Eng), units = "Eng")
Re_mu
```

<br />

Michael Lindeburg calculated the Reynolds number as `r round((8.33*0.5054)/(1.217e-05))` [rounded to $3.46e05$].

<br />
<br />
<br />
<br />

## Problem Statement

"This month's problem asked what flow rate of water would be needed to have fully developed turbulent flow in a circular tube." [Fosse]

What is the Reynolds number given the mass flow rate?

<br />
<br />

## Solution

```{r, warning = FALSE, message = FALSE, tidy = TRUE}
# Please note that the Re3 function is found within the iemisc R package created by Irucka Embry

# given temperature of 22 degrees Celsius
T <- 22
T

# create a numeric vector with the units of degrees Celsius
T_C <- set_units(T, degree_C)
T_C

# create a numeric vector to convert from degrees Celsius to Kelvin
T_K <- T_C
T_K

# create a numeric vector with the units of Kelvin
units(T_K) <- with(ud_units, K)
T_K

# saturated liquid density at 22 degrees Celsius (SI units)
rho_SI <- DfT(drop_units(T_K))
rho_SI <- rho_SI * as_units("kg/m^3")
rho_SI

# kinematic viscosity at 22 degrees Celsius and density of rho (SI units)
nu_SI <- KViscTD(drop_units(T_K), drop_units(rho_SI))
nu_SI <- nu_SI * as_units("m^2/s")
nu_SI

# absolute or dynamic viscosity at 22 degrees Celsius and density of rho (SI units)
mu_SI <- ViscTD(drop_units(T_K), drop_units(rho_SI))
mu_SI <- mu_SI * as_units("Pa*s")
mu_SI

# create a numeric vector with the units of inches for the given internal pipe diameter
Di <- 4 * as_units("in")
Di

# create a numeric vector with the units of meters for the given internal pipe diameter
Di <- set_units(Di, m)
Di

# internal area of the pipe
Ai <- Di ^ 2 * pi / 4
Ai

# given the mass flow rate of 0.765 kg/s (rounded in HTML)
G <- 0.76486004
G

# create a numeric vector with the units of kilograms per second for the mass flow rate
units(G) <- with(ud_units, "kg/s")
G

# in order to get the "mass flow rate per unit area", divide by the unit area (Ai)
G_use <- G / Ai
G_use

Re3(D = drop_units(Di), G = drop_units(G_use), mu = drop_units(mu_SI), units = "SI")
```

<br />

Kendall Fosse provided $1e04$ for the Reynolds number.

<br />
<br />
<br />
<br />

## Works Cited

Kendall Fosse, "What rate is needed for turbulent flow? [Challenge Solved]", ChEnected, https://www.aiche.org/chenected/2010/10/what-rate-needed-turbulent-flow-challenge-solved.

Michael R. Lindeburg, PE, *Civil Engineering Reference Manual for the PE Exam*, Twelfth Edition, Belmont, California: Professional Publications, Inc., 2011, page 17-4, 17-7, A-22.

Michael R. Lindeburg, PE, *Practice Problems for the Civil Engineering PE Exam: A Companion to the "Civil Engineering Reference Manual"*, Twelfth Edition, Belmont, California: Professional Publications, Inc., 2011, pages 17-1 and 17-7.

The NIST Reference on Constants, Units, and Uncertainty, Fundamental Constants Data Center of the NIST Physical Measurement Laboratory, "standard acceleration of gravity g_n", https://physics.nist.gov/cgi-bin/cuu/Value?gn.

Wikimedia Foundation, Inc. Wikipedia, 15 May 2019, “Conversion of units”, https://en.wikipedia.org/wiki/Conversion_of_units.

<br />
<br />

## EcoC^2^S Links

[EcoC&sup2;S Home](index.html)
<br />
[About EcoC&sup2;S](about_ecoc2s.html)
<br />
[EcoC&sup2;S Services]()
<br />
[Products](https://www.questionuniverse.com/products.html)
<br />
[EcoC&sup2;S Media](media.html)
<br />
[EcoC&sup2;S Resources](resources.html)
<br />
[R Trainings and Resources provided by EcoC&sup2;S (Irucka Embry, E.I.T.)](rtraining.html)

<br />
<br />

## Copyright and License

All R code written by Irucka Embry is distributed under the GPL-3 (or later) license, see the [GNU General Public License (GPL) page](https://gnu.org/licenses/gpl.html).

All written content originally created by Irucka Embry is copyrighted under the Creative Commons Attribution-ShareAlike 4.0 International License. All other written content retains the copyright of the original author(s).

![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
<br />
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
