% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/weighted_CN.R
\name{weighted_CN}
\alias{weighted_CN}
\title{Calculate the Weighted Curve Number}
\usage{
weighted_CN(CN = NULL, CN_area_table = NULL,
  CN_area_pct_table = NULL, area = NULL, area_pct = NULL,
  area_units = c("acre", "square feet", "square mile", "hectare",
  "square kilometer"))
}
\arguments{
\item{CN}{numeric vector containing dimensionless Curve Number(s)}

\item{CN_area_table}{data.frame/data.table/tibble, list, or matrix
containing the CN in column 1 and the area in column 2}

\item{CN_area_pct_table}{data.frame/data.table/tibble, list, or matrix
containing the CN in column 1 and the area_pct in column 2}

\item{area}{numeric vector containing the surface land area}

\item{area_pct}{numeric vector containing the surface land area, as a
percent (decimal or whole number)}

\item{area_units}{character vector containing the units for area
(default = "acres"). The units should be consistent and not mixed.}
}
\value{
the Weighted Curve Number factor as a single numeric vector, in the range [0, 100]
}
\description{
Calculate the Weighted Curve Number
}
\examples{
library("iemisc")
library("ramify")

# Note: the default area unit is acre

# Example 1
area1 <- c(220, 150, 30)
CN1 <- c(75, 89, 80)
weighted_CN(CN = C1, area = area1)


# Example 2
area2 <- c(220, 150, 30)
area_pct2 <- area2 / sum(area2)
CN2 <- c(80, 95, 80)
CN_area_pct_table2 <- data.frame(CN2, area_pct2)
weighted_CN(CN_area_pct_table = CN_area_pct_table2)


# Example 3
CN_area_table3 <- data.table(CN = c(98, 100, 45), area = c(2.53, 453.00, 0.21))
weighted_CN(CN_area_table = CN_area_table3)


# Example 4
CN4 <- c(98, 100, 45)
area_pct4 <- c(0.15, 0.23, 0.62)
weighted_CN(CN = C4, area_pct = area_pct4)


# Example 5
data_matrix5a <- matrix(c(98, 30, 40, 43, 57, 3.24, 1, 30, 50, 123), nrow = 5, ncol = 2, dimnames = list(rep("", 5), c("C", "Area")))
weighted_CN(CN_area_table = data_matrix5a)


# using ramify to create the  matrix
data_matrix5b <- mat("98 30 40 43 57;3.24 1 30 50 123", rows = FALSE, sep = " ", dimnames = list(rep("", 5), c("CN", "Area")))
weighted_CN(CN_area_table = data_matrix5b)


# Example 6 - using area in square feet
data_list6 <- list(CN = c(77, 29, 68), Area = c(43560, 56893, 345329.32))
weighted_CN(CN_area_table = data_list6, area_units = "square feet")


# Example 7 - using area in whole percents
CN7 <- c(61, 74)
area_pct7 <- c(30, 70)
weighted_CN(CN = CN7, area_pct = area_pct7)


}
