test_f7 <- function() {

library("iemisc")

Re <- 400000
eps <- 0.004
D <- 1

RUnit::checkEquals(f7(eps = eps, D = D, Re = Re), 0.0286979783)
RUnit::checkEquals(f7(eps = eps, Re = Re), 0.0286450321)
RUnit::checkException(f7(eps = eps, D = D, Re = 2000))
RUnit::checkException(f7(eps = 0.06, Re = Re))
  invisible(NULL)
}