test_f1 <- function() {

library("iemisc")

RUnit::checkEquals(f1(2000), 0.032)
RUnit::checkEquals(f1(0.00001), 6400000)
RUnit::checkException(f1("sq"))
RUnit::checkException(f1(2500))
  invisible(NULL)
}