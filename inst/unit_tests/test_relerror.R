test_relerror <- function() {

library("iemisc")

RUnit::checkEquals(relerror(1.648721, 1.5), 9.02038611)
RUnit::checkEquals(relerror(99.9, 100), 0.1001001)
RUnit::checkException(relerror(0))
RUnit::checkException(relerror("sq"))

  invisible(NULL)
}